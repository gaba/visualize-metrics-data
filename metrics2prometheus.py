#!/usr/bin/env python3
"""https://pad.riseup.net/p/2021-hackweek-metrics-viz"""
# TODO:
# - allow date cli args
# - allow configurtion file
# - allow other url args
# - make it a module
# - make it installable
# - make it async
import asyncio
import argparse
from csv import DictReader
import datetime
import logging
import time
import urllib.parse
import requests

from prometheus_client.core import GaugeMetricFamily, REGISTRY
from prometheus_client import push_to_gateway, start_http_server

DATE_FMT = "%Y-%m-%d"
METRIC_PREFIX = "tor_metrics_"
BASE_URL = "https://metrics.torproject.org/"
BASE_PATH = "data/"
METRICS = [
    {
        "name": METRIC_PREFIX + "bridge_user_count",
        "help": "foo",
        "value_key": "users",
        "path": BASE_PATH + "userstats-bridge-country.csv",
        "labels": ["country", "users"],
        "url": BASE_URL + "userstats-bridge-country.csv",
    },
    {
        "name": METRIC_PREFIX + "relay_user_count",
        "help": "foo",
        "value_key": "users",
        "path": BASE_PATH + "userstats-relay-country.csv",
        "labels": ["country", "users"],
        "url": BASE_URL + "userstats-relay-country.csv",
    },
    # {
    #     "name": METRIC_PREFIX + "bridge_cobiend_count",
    #     "help": "foo",
    #     "value_key": "users",
    #     "path": BASE_PATH + "userstats-bridge-combined.csv",
    #     "labels": ["country", "transport"],
    #     "url": BASE_URL + "userstats-bridge-combined.csv",
    # },
]
PUSH_GATEWAY_ADDRESS = "127.0.0.1:9091"
PUSH_GATEWAY_JOB = "dev-push-gateway"
RUN_TIME_HOUR = 0

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
today = datetime.datetime.utcnow().replace(
    hour=0, minute=0, second=0, microsecond=0
)
three_days_ago = today - datetime.timedelta(days=3)


async def wait_until(dt):
    # sleep until the specified datetime
    now = datetime.datetime.now()
    await asyncio.sleep((dt - now).total_seconds())


async def run_at(dt, coro):
    await wait_until(dt)
    return await coro

def format_url(url, start=None, end=None):
    if not start:
        start = three_days_ago.strftime(DATE_FMT)
    if not end:
        end = today.strftime(DATE_FMT)
    params = urllib.parse.urlencode({"start": start, "end": end})
    return url + "?{}".format(params)


def format_csv_path(path, start=None, end=None):
    if not start:
        start = three_days_ago.strftime(DATE_FMT)
    if not end:
        end = today.strftime(DATE_FMT)
    path = path.split(".")[0]  + "-" + "-".join([start, end]) + ".csv"
    return path


def fetch(url):
    logger.info("Fetching url %s", url)
    response = requests.get(url)
    encoding = response.encoding
    filename = (
        response.headers.get("Content-Disposition")
        .rsplit(";")[1]
        .split("=")[1]
        .strip('"')
    )
    text = response.text
    print(text)
    lines = text.split("\n")
    lines = [line for line in lines if not line.startswith("#")]
    with open(BASE_PATH + filename, "w") as fd:
        logger.info("Writing %s", BASE_PATH + filename)
        fd.write("\n".join(lines))


class CustomCollector(object):
    def __init__(self):
        super().__init__()
        self.gauges = []

    def add_gauge_from_csv(self, metric, start=None, end=None):
        gauge = GaugeMetricFamily(
            metric["name"], metric["help"], labels=metric["labels"]
        )
        path = format_csv_path(metric["path"], start, end)
        with open(path, encoding="ISO-8859-1") as fd:
            csv_dict_reader = DictReader(fd)
            for row in csv_dict_reader:
                labels_values = []
                print(row.get("country", None), row.get("users", None))
                for label in metric["labels"]:
                    labels_values.append(row.get(label, None))
                gauge.add_metric(labels_values, row[metric["value_key"]])
        self.gauges.append(gauge)

    def collect(self):
        for gauge in self.gauges:
            yield gauge


def create_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument("-f", "--fetch", action="store_true")
    parser.add_argument(
        "-d", "--debug", help="Set logging level to debug", action="store_true"
    )
    parser.add_argument("-p", "--push-gateway", action="store_true")
    # parser.add_argument("-n", "--node-exporter")
    return parser


def run():
    parser = create_parser()
    args = parser.parse_args()

    if args.fetch:
        for metric in METRICS:
            url = format_url(metric["url"])
            fetch(url)
    collector = CustomCollector()
    for metric in METRICS:
        collector.add_gauge_from_csv(metric)

    if args.push_gateway:
        REGISTRY.register(collector)
        try:
            push_to_gateway(
                PUSH_GATEWAY_ADDRESS, job=PUSH_GATEWAY_JOB, registry=REGISTRY
            )
        except urllib.error.HTTPError as e:
            logger.critical(e)
    else:
    # if args.node_exporter:
        logger.info("Starting http server on port 8000")
        start_http_server(8000)
        REGISTRY.register(collector)
        while True:
            time.sleep(1)

# To leave it running and execute tasks at a certain time
# async def main():
#     run()

# if __name__ == "__main__":
#     next_run = today.replace(hour=0,minute=10,)
#     loop = asyncio.get_event_loop()
#     # print hello ten years after this answer was written
#     loop.create_task(run_at(next_run, main()))
#     loop.run_forever()

if __name__ == "__main__":
    run()
