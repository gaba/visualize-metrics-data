#!/usr/bin/env python3
from csv import DictReader
from prometheus_client import push_to_gateway
from prometheus_client.core import GaugeMetricFamily, REGISTRY
import requests, subprocess

from datetime import datetime

# THIS IS ONLY FOR Tor USER data

def get_countries():
    countries_file = open("data/countries.txt", "r")
    countries = (countries_file.read()).split("\n")
    countries.pop()
    return countries

def save_last_csv(url, user_data_file):
    r = requests.get(url, allow_redirects=True)
    open(user_data_file, 'wb').write(r.content)

class CustomCollector(object):

    def collect(self):
        g = GaugeMetricFamily('tor_metrics_user_count', 'Tor users per country per date', labels=['country', 'date'])

        # THE DATA we need IS FOR THE LAST DAY FOR EACH COUNTRY

        # users for each country for ALL times. TODO: we need to get only last measurements and not all data.,
        #url = 'https://metrics.torproject.org/userstats-relay-country.csv'

        countries = get_countries()
        start_date = '2021-03-01' # TODO get a month ago date
        end_date =  '2021-03-31' # TODO get today ago date
        # for each country, save data from last month
        # URL: https://metrics.torproject.org/userstats-relay-country.csv?start=2021-03-01&end=2021-03-31&country=all&events=off
        for country in countries:
            url = 'https://metrics.torproject.org/userstats-relay-country.csv?start=%s&end=%s&country=%s&events=off' % (start_date, end_date, country)
            user_data_file = 'data/user-data-%s.csv' % country
            save_last_csv(url, user_data_file)

            # removes the first 5 lines
            # sed -i '1,5d' userstats-relay-country.csv
            subprocess.call(["sed", "-i", "1,5d", user_data_file])

            with open(user_data_file, encoding="utf-8") as csvfile:
                csv_dict_reader = DictReader(csvfile)

                # row is {'date': '2021-03-10', 'country': 'zw', 'users': '10', 'frac': '73'}
                for row in csv_dict_reader:
                    #ts = datetime.strptime(row.get("date"),"%Y-%m-%d").timestamp()
                    g.add_metric([row.get("country", ""), row.get("date", "")], int(row.get("users", 0)))#, timestamp=ts)
        yield g

REGISTRY.register(CustomCollector())
push_to_gateway('localhost:9091', job='dev-push-gateway-users-stats', registry=REGISTRY)


# bridge users for each country for ALL times. TODO: we need to get only last measurements and not all data.
# url = 'https://metrics.torproject.org/userstats-bridge-country.csv'

# bridge users for each country for ALL times by transport. TODO: we need to get only last measurements and not all data.
# url = 'https://metrics.torproject.org/userstats-bridge-combined.csv'
